# [Backstage](https://backstage.io)

The is a platform that helps Developers developing their application without any concern.

## Prerequisites
1. `NodeJS` version 18 or 20
	- Install [`nvm`](https://github.com/nvm-sh/nvm#install--update-script):
	```sh
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
	```
	- Install Node version:
	```sh
	nvm install {version}
	```
	- Change Node version:
	```sh
	nvm use {version}
	```
2. `yarn` installation:
```sh
npm install -g yarn
```
3. `docker` [installation](https://docs.docker.com/engine/install/)

## Repository structure
This repository is organized as the following structure:

```sh
.
|- packages			# contains UI and backend code for Backstage app
|- plugins			# contains all plugins used in Backstage app
|- presentation		# contains .pptx presentation about Backstage
|- resources		# contains all Backstage resources, such as templates, users, groups, ...
	|- template
		|- react_app_on_s3		# sample Web app self-service
			|- template.yaml	# defines steps of self-service
			|- skeleton
				|- .github		# defines CI/CD pipeline running on Github Actions
				|- react		# Javascript code to develop Webapp
				|- s3			# Terraform code to provision a S3 bucket
		|- ...
|- ...
```

## How to run the Backstage app
To start the app, run:
```sh
yarn install
yarn dev
```
